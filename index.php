<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'app/mvc.php';

$app = new Slim\App;
$mvc = new MVC;

$container = $app->getContainer();
$container['renderer'] = new Slim\Views\PhpRenderer("./templates");

$app->get('/', function (Request $request, Response $response) {
    return $this->renderer->render($response, 'grid.php');
});

$app->get('/dane/', function (Request $request, Response $response) use ($mvc) {
    return $response->withJson($mvc->all());
});

$app->post('/dane/', function (Request $request, Response $response) use ($mvc) {
    $form = $request->getParsedBody();
    return $mvc->post($form['imie_nazwisko'], $form['data_od'], $form['data_do']);
});

$app->get('/dane/{id}', function (Request $request, Response $response, $params) use ($mvc) {
    return $response->withJson($mvc->get($params['id']));
});

$app->put('/dane/{id}', function (Request $request, Response $response, $params) use ($mvc) {
    $form = $request->getParsedBody();
    return $mvc->put($params['id'], $form['imie_nazwisko'], $form['data_od'], $form['data_do']);
});

$app->delete('/dane/{id}', function (Request $request, Response $response, $params) use ($mvc) {
    return $mvc->delete($params['id']);
});

$app->run();