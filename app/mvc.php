<?php
class MVC
{
    const GET_ALL = 'select id, imie_nazwisko, data_od, data_do from dane.dane';
    
    const GET_ONE = 'select id, imie_nazwisko, data_od, data_do
                     from dane.dane
                     where id = :id';
    
    const INSERT = 'insert into dane.dane (imie_nazwisko, data_od, data_do)
                    values (:imie_nazwisko, :data_od, :data_do)';
        
    const UPDATE = 'update dane.dane set
                    imie_nazwisko = :imie_nazwisko,
                    data_od = :data_od,
                    data_do = :data_do
                    where id = :id';
    
    const DELETE = 'delete from dane.dane where id = :id';
    
    public function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;port=3306', 'root', 'root');
    }
    
    public function all()
    {
        return $this->db->query(self::GET_ALL)->fetchAll(PDO::FETCH_NUM);
    }
    
    public function get($id)
    {
        $stmt = $this->db->prepare(self::GET_ONE);
        $stmt->execute([':id' => $id]);
        return $stmt->fetch(PDO::FETCH_NUM);
    }
    
    public function post($imie_nazwisko, $data_od, $data_do)
    {
        $params = [
            ':imie_nazwisko' => $imie_nazwisko,
            ':data_od' => $data_od,
            ':data_do' => $data_do
        ];

        return $this->db->prepare(self::INSERT)->execute($params);
    }
    
    public function put($id, $imie_nazwisko, $data_od, $data_do)
    {
        $params = [
            ':imie_nazwisko' => $imie_nazwisko,
            ':data_od' => $data_od,
            ':data_do' => $data_do,
            ':id' => $id
        ];
        
        return $this->db->prepare(self::UPDATE)->execute($params);
    }
    
    public function delete($id)
    {
        $params = [
            ':id' => $id
        ];
        
        return $this->db->prepare(self::DELETE)->execute($params);
    }
    
    private $db;
}