<style>
table {
	width: 100%;
}
</style>
<link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
    $.get('dane/', function(data){
        for (var i=0; i<data.length; ++i) {
			add_row(data[i]);
        }
    });

    $("#dodaj").on("click", function(){
        $.ajax({
            url: 'dane/',
            method: 'POST',
            data: get_data(),
            success: function(){
                window.location.reload();
            }
        });
    });
});

function add_row (row)
{
    var row_id = row[0];
    var row_elem = $('<tr/>)');
    row_elem.append($('<td/>').text(row[1]));
    row_elem.append($('<td/>').text(row[2]));
    row_elem.append($('<td/>').text(row[3]));

	var button_delete = $('<button class="btn btn-default"/>').text('Usuń');
	button_delete.on('click', function(){
		$.ajax({
			url: 'dane/' + row_id,
			method: 'DELETE',
			success: function(){
				window.location.reload();
			}
		});
	});
	row_elem.append(button_delete);

	var button_edit = $('<button class="btn btn-default"/>').text('Modyfikuj');
	button_edit.on("click", function(){
		$.ajax({
			url: 'dane/' + row_id,
			method: 'PUT',
			data: get_data(),
			success: function(){
				window.location.reload();
			}
		});
	});
	row_elem.append(button_edit);

    $('table').append(row_elem);
}

function get_data(){
    return {
        imie_nazwisko: prompt('podaj imię i nazwisko'),
        data_od: prompt('podaj datę początkową'),
        data_do: prompt('podaj datę końcową')
    };
}
</script>
<button id="dodaj" class='btn btn-primary'>Dodaj</button>
<table class='table-stripped'>
    <thead>
        <tr>
            <th>Imię i nazwisko</th>
            <th>Data od</th>
            <th>Data do</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>